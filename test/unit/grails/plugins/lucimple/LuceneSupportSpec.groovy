package grails.plugins.lucimple

import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.store.RAMDirectory

import grails.plugins.lucimple.analysis.DigitsAwareSimpleAnalyzer
import grails.plugins.lucimple.builder.ReusableDocumentBuilder
import spock.lang.Shared
import spock.lang.Specification

class LuceneSupportSpec extends Specification {
  
  @Shared RAMDirectory rd = new RAMDirectory()
  
  @Shared IndexWriter w = new IndexWriter( rd, new IndexWriterConfig( IndexOpsService.version, new DigitsAwareSimpleAnalyzer() ) )
  
  @Shared IndexSearcher s = new IndexSearcher( w.reader )

	void "test each term"() {
    given:
    w.deleteAll()
    ReusableDocumentBuilder b = new ReusableDocumentBuilder()
    b{
      aaa( [ 'ccc', 'ddd', 'eee' ] )
      bbb( [ 111, 222, 333 ] )
    }
    w.addDocument b.doc
    b.reset()
    b{
      aaa( [ 'xxx', 'yyy', 'eee' ] )
      bbb( [ 888, 999, 111 ] )
    }
    w.addDocument b.doc
    w.commit()
    def res = []
    LuceneSupport.eachTerm( w.reader, field ){ text, freq, ix -> res << "$text:$freq" } 
    
    expect:
    result == res
    
    where:
    field   |   result
    'aaa'   | [ 'ccc:1', 'ddd:1', 'eee:2', 'xxx:1', 'yyy:1' ]
    'bbb'   | [ '111:2', '222:1', '333:1', '888:1', '999:1' ]
    'fake'  | []
	}
  
	void "test each prefixed term"() {
	  given:
    w.deleteAll()
    ReusableDocumentBuilder b = new ReusableDocumentBuilder()
  	b{
  	  fff( [ 'www_www', 'www_yyy', 'aaa_bbb_ccc', 'aaa_bbb_ddd', 'aaa_fff_eee', 'ggg_hhh_kkk' ] )
  	}
  	w.addDocument b.doc
  	w.commit()
    def res = []
    LuceneSupport.eachPrefixedTerm( w.reader, field, prefix, '_' ){ text, freq, ix -> res << "$text:$freq" } 
	
    expect:
	  result == res
	  
	  where:
    field   | prefix        |  result
    'fff'   | ''            | [ 'aaa:3', 'ggg:1', 'www:2' ]
    'fff'   | 'aaa'         | [ 'aaa_bbb:2', 'aaa_fff:1' ]
    'fff'   | 'aaa_bbb'     | [ 'aaa_bbb_ccc:1', 'aaa_bbb_ddd:1' ]
    'fff'   | 'ggg_hhh_kkk' | [ 'ggg_hhh_kkk:1' ]
    'fake'  | 'aaa'         | []
    'fff'   | 'xxx'         | []
	}
  
}