package grails.plugins.lucimple.builder

import grails.plugins.lucimple.analysis.DigitsAwareSimpleAnalyzer
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.core.SimpleAnalyzer
import org.apache.lucene.analysis.core.WhitespaceAnalyzer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.lucene.document.Field
import org.apache.lucene.document.DateTools.Resolution
import org.junit.Test

class ReusableDocumentBuilderTest {

  @Test
  public void testBuild() {
    def d = new ReusableDocumentBuilder( defaultStore:false )
    d{
      compositeKey 1111111111
      compositeKey 2222222222l
      cumulativeRating value:[ '10', '20' ], store:false
    }
    assert d.doc.toString() == 'Document<indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:1111111111> indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:2222222222> indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:10> indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:20>>'
    d.reset()
    d{
      cumulativeRating value:10, store:false
      cumulativeRating value:20, store:false
    }
    assert d.doc.toString() == 'Document<indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:> indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:10> indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:20>>'
    d.reset()
    d{
      compositeKey '1111111111'
      compositeKey '2222222222'
      cumulativeRating value:'10', store:false
    }
    assert [ '1111111111', '2222222222' ] == d.doc.getFields( 'compositeKey' )*.stringValue()
    assert '10' == d.doc.getField( 'cumulativeRating' ).stringValue()
    d.reset()
    d{
      compositeKey '1111111111'
      cumulativeRating value:[ '10', '20' ], store:true
    }
    assert '1111111111' == d.doc.getField( 'compositeKey' ).stringValue()
    assert [ '10', '20' ] == d.doc.getFields( 'cumulativeRating' )*.stringValue()
  }
  
  @Test
  public void testBuildBinary() {
    def d = new ReusableDocumentBuilder()
    def b = [1,2,3,4,5] as byte[]
    d{
      img b
    }
    assert d.doc.toString() == 'Document<stored<img:[1 2 3 4 5]>>'
    d.reset()
    assert d.doc.toString() == 'Document<stored<img:[]>>'
    d{ img b }
    assert d.doc.toString() == 'Document<stored<img:[1 2 3 4 5]>>'
  }
  
  @Test
  public void testSimpleList() {
    def d = new ReusableDocumentBuilder()
    def vals = [ 111, 222, 333 ]
    d{
      aaa vals
    }
    assert vals == d.doc.getFields( 'aaa' )*.stringValue()*.toInteger()
  }
  
  @Test
  public void testBoost() {
    def d = new ReusableDocumentBuilder()
    d{
      compositeKey value:'1111111111', boost:20, index:true
      someOtherKey value:'1111111111', boost:10, index:'aaaa 111'
    }
    def f = d.doc.getFields( 'compositeKey' ).find{ it.fieldType().indexed() }
    assert 20 == f.boost
    f = d.doc.getFields( 'someOtherKey' ).find{ it.fieldType().indexed() }
    assert 10 == f.boost
  }

  @Test
  public void testNumericFields() {
    def d = new ReusableDocumentBuilder( defaultStore:false )
    d{
      someInt int:1
      someLong long:2222
      someFloat float:22.22f
      someDouble double:33.33d
    }
    assert d.doc.getField( 'someInt' ).numericValue() == 1
    assert d.doc.getField( 'someLong' ).numericValue() == 2222l
    assert d.doc.getField( 'someFloat' ).numericValue() == 22.22f
    assert d.doc.getField( 'someDouble' ).numericValue() == 33.33d
    d.reset()
    assert d.doc.getField( 'someInt' ).numericValue() == 0
    assert d.doc.getField( 'someLong' ).numericValue() == 0l
    assert d.doc.getField( 'someFloat' ).numericValue() == 0f
    assert d.doc.getField( 'someDouble' ).numericValue() == 0d
    d{
      someInt int:[ 33, 44 ]
    }
    assert d.doc.getFields( 'someInt' )*.numericValue() == [ 33, 44 ]
    assert d.doc.getField( 'someLong' ).numericValue() == 0l
    assert d.doc.getField( 'someFloat' ).numericValue() == 0f
    assert d.doc.getField( 'someDouble' ).numericValue() == 0d
  }
  
  @Test
  public void testDate() {
    def d = new ReusableDocumentBuilder()
    Date n = new Date( 0 )
    d{
      min date:n, resolution:Resolution.MINUTE
      hour date:n, resolution:'hour'
      month date:n, resolution:'Month'
      defolt date:n
    }
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<min:197001010000> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<hour:1970010100> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<month:197001> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<defolt:19700101>>'
    d.reset()
    d{
      month date:n + 1000, resolution:Resolution.MONTH
    }
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<min:> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<hour:> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<month:197209> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<defolt:>>'
  }
  
  @Test
  public void testBuildTokenStream() {
    String v = 'Ukraine : Odesskaya oblast11 : Odessa : 222'
    def a = { new DigitsAwareSimpleAnalyzer() }
    TokenStream ts = a().tokenStream( 'dummy', new StringReader( v ) )
    def sb = new StringBuilder()
    ts.reset()
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    final String ethalon = sb.toString()
    
    def d = new ReusableDocumentBuilder()
    d{
      compositeKey '1111111111'
      simple a().tokenStream( 'simple', new StringReader( v ) )
    }
    ts = d.doc.getField( 'simple' ).tokenStreamValue()
    ts.reset()
    sb = new StringBuilder()
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert sb.toString() == ethalon
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:1111111111> indexed,tokenized<simple:>>'

    d.reset()
    d{
      compositeKey '1111111111'
    }
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:1111111111> indexed,tokenized<simple:>>'
    
    d = new ReusableDocumentBuilder()
    d{
      compositeKey '1111111111'
      simple value:v, index:a().tokenStream( 'simple', new StringReader( v ) )
      noValueMap index:a().tokenStream( 'noValueMap', new StringReader( v ) )
      indexOnly a().tokenStream( 'indexOnly', new StringReader( v ) )
    }
    for( fieldName in [ 'simple', 'noValueMap', 'indexOnly' ] ){
      ts = d.doc.getField( fieldName ).tokenStreamValue()
      ts.reset()
      sb.length = 0
      while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
      assert sb.toString() == ethalon
    }
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:1111111111> stored,indexed,tokenized<simple:Ukraine : Odesskaya oblast11 : Odessa : 222> indexed,tokenized<noValueMap:> indexed,tokenized<indexOnly:>>'
    
    d.reset()
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:> stored,indexed,tokenized<simple:> indexed,tokenized<noValueMap:> indexed,tokenized<indexOnly:>>'
    d = new ReusableDocumentBuilder()
    d{
      whitespace value:'aaa bbb ccc', index:'ddd   fff    ggg'
      ws2 value:'aaa bbb ccc', index:[ value:'ddd   fff    ggg' ]
      standard value:'aaa bbb ccc', index:[ value:'the wind blows a boy and an eagles away', analyzer:'en' ]
      ws3 index:[ value:'ddd   fff    ggg' ]
      ws4 value:'aaa bbb ccc', index:true
      ws5 value:'aaa bbb ccc', index:false
      ws6 value:'aaa bbb ccc', index:true, store:false
      ignored store:true
    }
    assert d.doc.toString() == 'Document<stored,indexed,tokenized<whitespace:aaa bbb ccc> stored,indexed,tokenized<ws2:aaa bbb ccc> stored,indexed,tokenized<standard:aaa bbb ccc> indexed,tokenized<ws3:> stored,indexed,tokenized<ws4:aaa bbb ccc> stored<ws5:aaa bbb ccc> indexed,tokenized<ws6:aaa bbb ccc>>'
    sb.length = 0
    ts = d.doc.getField( 'standard' ).tokenStreamValue()
    ts.reset()
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert 'wind|blows|boy|eagles|away|' == sb.toString()
    
    for( fieldName in [ 'whitespace', 'ws2', 'ws3' ] ){
      Field f = d.doc.getField( fieldName )
      ts = f.tokenStreamValue()
      ts.reset()
      sb.length = 0
      while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
      assert sb.toString() == 'ddd|fff|ggg|'
    }
  }
  @Test
  public void testComplexIndex() {
    def d = new ReusableDocumentBuilder()
    def sb = new StringBuilder()
    TokenStream ts
    d{
      w value:'aaa bbb ccc', index:'ddd   fff    ggg'
    }
    assert d.doc.toString() == 'Document<stored,indexed,tokenized<w:aaa bbb ccc>>'
    Field f = d.doc.getField( 'w' )
    ts = f.tokenStreamValue()
    ts.reset()
    sb.length = 0
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert sb.toString() == 'ddd|fff|ggg|'
    
    d.reset()
    d{
      w value:'xxx yy', index:'zzzz uuuu'
    }
    assert d.doc.toString() == 'Document<stored,indexed,tokenized<w:xxx yy>>'
    f = d.doc.getField( 'w' )
    ts = f.tokenStreamValue()
    ts.reset()
    sb.length = 0
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert sb.toString() == 'zzzz|uuuu|'
  }

  @Test
  public void testReset() {
    def d = new ReusableDocumentBuilder()
    d{
      compositeKey '1'
      compositeKey '2'
      compositeKey '3'
      cumulativeRating value:'10', store:false
    }
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:1> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:2> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:3> indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:10>>'
    d.reset()
    d{
      compositeKey( [ '1111111111', '2222222222' ] )
      cumulativeRating value:'10', store:false
    }
    assert d.doc.toString() == 'Document<stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:1111111111> indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:10> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:2222222222>>'
    d.reset()
    d{
      compositeKey '333333333'
      cumulativeRating value:'33333333', store:false
    }
    assert d.doc.toString() == 'Document<indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:33333333> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:333333333>>'
    
    d.reset()
    d{
      compositeKey '333333333'
    }
    assert d.doc.toString() == 'Document<indexed,omitNorms,indexOptions=DOCS_ONLY<cumulativeRating:> stored,indexed,omitNorms,indexOptions=DOCS_ONLY<compositeKey:333333333>>'
  }
  
  @Test
  public void testMultipleFloat() {
    def d = new ReusableDocumentBuilder()
    d{
      price float:11.11
      price float:22.22d
      price float:33.33d
      price float:44.44
    }
    assert d.doc.getFields( 'price' )*.numericValue() == [ 11.11f, 22.22f, 33.33f, 44.44f ]
    d.reset()
    assert d.doc.getFields( 'price' )*.numericValue() == [ 0f ]
    d{
      price float:11.11, store:false
    }
    assert d.doc.toString() == 'Document<stored,indexed,tokenized,omitNorms,indexOptions=DOCS_ONLY,numericType=FLOAT,numericPrecisionStep=4<price:11.11>>'
   }
  
  @Test
  public void testAnalyze() {
    def sb = new StringBuilder()
    TokenStream ts
    def d = new ReusableDocumentBuilder()
    d{
      simple value:'müller zürich', analyzer:'simple'
      de value:'müller zürich', analyzer:'de'
    }
    assert d.doc.toString() == 'Document<stored,indexed,tokenized<simple:müller zürich> stored,indexed,tokenized<de:müller zürich>>'
    Field f = d.doc.getField( 'de' )
    ts = f.tokenStreamValue()
    ts.reset()
    sb.length = 0
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert sb.toString() == 'mull|zurich|'
    f = d.doc.getField( 'simple' )
    ts = f.tokenStreamValue()
    ts.reset()
    sb.length = 0
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert sb.toString() == 'müller|zürich|'
    
    d.reset()
    d{
      de value:'öäüß', index:'öäüß', analyzer:'de'
    }
    assert d.doc.toString() == 'Document<stored,indexed,tokenized<simple:> stored,indexed,tokenized<de:öäüß>>'
    f = d.doc.getField( 'de' )
    ts = f.tokenStreamValue()
    ts.reset()
    sb.length = 0
    while( ts.incrementToken() ) sb << ts.getAttribute( CharTermAttribute ) << '|'
    assert sb.toString() == 'oauss|'
  }

}
