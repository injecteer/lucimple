package grails.plugins.lucimple.builder

import org.apache.lucene.document.DateTools.Resolution
import org.apache.lucene.index.Term
import org.apache.lucene.search.*
import org.junit.Test

import static org.junit.Assert.*

class FilterBuilderTest {

  @Test
  public void testBuild() {
    def types = [ 111, 222, 4444 ]
    def f = new FilterBuilder().call{
      id 11111
      all id:9999, aaa:[ 888, 777, 666 ]
      any id:111, aaa:[ 222, 333, 444 ]
    }
    assert f.toString() == 'BooleanFilter(+id:11111 +<+id:9999 +aaa:888 +aaa:777 +aaa:666> +aaa:222 aaa:333 aaa:444 id:111)'
    
    f = new FilterBuilder().call{
      group{
        not{ 
          all id:333, aaa:[ 888, 777, 666 ]
          any id:444, aaa:[ 888, 777, 666 ] 
        } 
      }
    }  
    assert f.toString() == 'BooleanFilter(+BooleanFilter(-<+id:333 +aaa:888 +aaa:777 +aaa:666> -aaa:666 aaa:777 aaa:888 id:444))'
    
    f = new FilterBuilder().call{
      not{ 
        group{
          all id:333, aaa:[ 888, 777, 666 ]
          any id:444, aaa:[ 888, 777, 666 ] 
        } 
      }
    }  
    assert f.toString() == 'BooleanFilter(-BooleanFilter(+<+id:333 +aaa:888 +aaa:777 +aaa:666> +aaa:666 aaa:777 aaa:888 id:444))'
    
    f = new FilterBuilder().call{
      not{ 
        group{
          or{
            all id:333, aaa:[ 888, 777, 666 ]
            any id:444, aaa:[ 888, 777, 666 ]
          } 
        } 
      }
    }  
    assert f.toString() == 'BooleanFilter(-BooleanFilter(<+id:333 +aaa:888 +aaa:777 +aaa:666> aaa:666 aaa:777 aaa:888 id:444))'
    
    f = new FilterBuilder().call{
      or{ 
        range field:'bbb', value:'aa'
        range field:'bbb', from:'a', to:'c'
      }
      group{
        or{
          prefixed pref:'pppp', zzzz:null
          numericFloat field:'ccc', from:11.4f, to:15f
          uids types
        }
      }
    }
    assert f.toString() == 'BooleanFilter(bbb:[aa TO aa] bbb:[a TO c] +BooleanFilter(PrefixFilter(pref:pppp) ccc:[11.4 TO 15.0] uids:111 uids:222 uids:4444))'
          
    f = new FilterBuilder().call{
      not{
        filter new PrefixFilter( new Term( 'HHHH', 'prefix' ) )
      }
      prefixed fff:'aaa', ttt:222
    }
    assert f.toString() == 'BooleanFilter(-PrefixFilter(HHHH:prefix) +PrefixFilter(fff:aaa) +PrefixFilter(ttt:222))'
  }
  
  @Test
  public void testDateRange() {
    Date d = new Date( 0 )
    def f = new FilterBuilder().call{
      dateRange field:'dateSingle', value:d
      dateRange field:'dateDefault', from:d, to:d + 10
      dateRange field:'dateResolution', from:d, to:d + 1, resolution:Resolution.MINUTE
    }
    assert f.toString() == 'BooleanFilter(+dateSingle:[19700101 TO 19700101] +dateDefault:[19700101 TO 19700111] +dateResolution:[197001010000 TO 197001020000])'
  }

}
