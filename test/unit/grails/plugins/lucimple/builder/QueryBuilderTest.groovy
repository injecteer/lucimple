package grails.plugins.lucimple.builder

import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.DateTools.Resolution;
import org.apache.lucene.index.Term
import org.apache.lucene.search.*
import org.junit.Test

import static org.junit.Assert.*

class QueryBuilderTest {

  @Test
  public void testBuild() {
    def types = [ 111, 222, 4444 ]
    def q = new QueryBuilder().call{
      id 11111
      all id:111, aaa:[ 11, 22 ]
      any id:222, aaa:[ 33, 33 ]
      not{ 
        all id:333, aaa:[ 88, 77 ]
        any id:444, aaa:[ 666, 777 ] 
      }
    }
    assert q.toString() == '+id:11111 +(+id:111 +aaa:11 +aaa:22) +(id:222 aaa:33 aaa:33) -(+id:333 +aaa:88 +aaa:77) -(id:444 aaa:666 aaa:777)'
    
    q = new QueryBuilder().call{
      range field:'bbb', value:'aa'
      range field:'bbb', from:'a', to:'c'
      or{ 
        range field:'bbb', value:'aa'
        range field:'bbb', from:'a', to:'c'
      }
      group{
        not{ 
          range field:'bbb', value:'aa'
          range field:'bbb', from:'a', to:'c'
        }
      }
    }
    assert q.toString() == '+bbb:[aa TO aa] +bbb:[a TO c] bbb:[aa TO aa] bbb:[a TO c] +(-bbb:[aa TO aa] -bbb:[a TO c])'
    
    q = new QueryBuilder().call{
      group{
        or{
          prefixed pref:'pppp', zzzz:null // check for null value
          numericFloat field:'ccc', from:11.4f, to:15f
        }
      }
      not{
        uids types
      }
      nulls null
      or{ 
        all id:types
      } 
      any uids:types 
      id 111
    }
    assert q.toString() == '+(pref:pppp* ccc:[11.4 TO 15.0]) -uids:111 -uids:222 -uids:4444 (+id:111 +id:222 +id:4444) +(uids:111 uids:222 uids:4444) +id:111'

    q = new QueryBuilder().call{
      not{
        query new PrefixQuery( new Term( 'HHHH', 'prefix' ) )
      }
      prefixed field:'aaa', text:222
    }
    assert q.toString() == '-HHHH:prefix* +field:aaa* +text:222*'
  }
  
  @Test
  public void testDateRange() {
    Date now = new Date()
    String from = DateTools.dateToString( now, Resolution.DAY )
    String to = DateTools.dateToString( now + 10, Resolution.DAY )
    def q = new QueryBuilder().call{
      dateRange field:'date', from:now, to:now + 10
    }
    assert q.toString() == "+date:[$from TO $to]"
  }

}
