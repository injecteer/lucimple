package grails.plugins.lucimple

import static org.junit.Assert.*
import grails.plugins.lucimple.RedundancyCollector;

import org.junit.Test

class RedundancyCollectorTest {
  
  @Test
  void testAsBoolean(){
    def rc = new RedundancyCollector( prefix:'X' )
    assert !rc
    rc << 'a'
    rc.flush()
    assert rc
    rc.reset()
    assert !rc
  }
  
  @Test
  void testAdd(){
    def rc = new RedundancyCollector()
    rc << 'X'
    rc << [ 1, 2 ]
    rc << [ 'a', 'b' ]
    rc << 'F'
    assert 4 == rc.data.size()
    assert '[[X, 1, a, F], [X, 2, a, F], [X, 1, b, F], [X, 2, b, F]]' == rc.data.toString()

    rc.reset()
    rc << [ 1, 2 ]
    rc << [ 'a', 'b', 'c' ]
    assert 6 == rc.data.size()
    assert '[[1, a], [2, a], [1, b], [2, b], [1, c], [2, c]]' == rc.data.toString()

    rc.reset()
    rc << [ 1, 2, 3]
    rc << [ 'a', 'b', 'c' ]
    assert 9 == rc.data.size()
    assert '[[1, a], [2, a], [3, a], [1, b], [2, b], [3, b], [1, c], [2, c], [3, c]]' == rc.data.toString()
  }
  
  @Test
  void testCombine(){
    def rc = new RedundancyCollector( prefix:'X' )
    rc << 'a'
    rc << 'b'
    rc << 'c'
    rc.combine()
    
    assert """X|a|_|_
X|_|b|_
X|a|b|_
X|_|_|c
X|a|_|c
X|_|b|c
X|a|b|c""" == rc.result.toString().trim()
    
    rc.reset()
    rc.data = [ [ 'a' ] ]
    rc.combine()
    assert 'X|a' == rc.result.toString().trim()
    
    
    rc.reset()
    rc << 'a'
    rc << 'b'
    rc << [ 'c' ]
    rc << 'd'
    rc.combine()
    assert """X|a|_|_|_
X|_|b|_|_
X|a|b|_|_
X|_|_|c|_
X|a|_|c|_
X|_|b|c|_
X|a|b|c|_
X|_|_|_|d
X|a|_|_|d
X|_|b|_|d
X|a|b|_|d
X|_|_|c|d
X|a|_|c|d
X|_|b|c|d
X|a|b|c|d""" == rc.result.toString().trim()


    rc.reset()
    rc.allowAllEmpty = true
    rc << 'a'
    rc << 'b'
    rc << null
    rc << ''
    rc.combine()
  assert """X|_|_|_|_
X|a|_|_|_
X|_|b|_|_
X|a|b|_|_""" == rc.result.toString().trim()

  rc.reset()
  rc.allowAllEmpty = false
  rc << 'a'
  rc << 'b'
  rc << null
  rc << ''
  rc.combine()
  assert """X|a|_|_|_
X|_|b|_|_
X|a|b|_|_""" == rc.result.toString().trim()

  rc.reset()
  rc.clean = true
  rc << 'a a' << '1)(*_'
  rc.combine()
  assert """X|aa|_
X|_|1_
X|aa|1_""" == rc.result.toString().trim()

  rc.reset()
  rc.hashLines = true
  rc << 'a' << 'b' << 'c'
  rc.combine()
  assert """139330883
137486724
139333766
137483845
139330887
137486728
139333770
""" == rc.result.toString()
  }
  
  @Test
  void testMerge(){
    def rc = new RedundancyCollector()
    rc << [ 'a' ]
    rc << null
    rc << 'b'
    assert 1 == rc.data.size()
    assert [ '|a|_|b' ] == rc.merge()
    
    rc.reset()
    rc.prefix = 'X'
    rc << 'a'
    rc << [ 1, 2 ]
    rc << [ 'b', 'c', 'd' ]
    assert 6 == rc.data.size()
    assert [ 'X|a|1|b', 'X|a|2|b', 'X|a|1|c', 'X|a|2|c', 'X|a|1|d', 'X|a|2|d' ] == rc.merge()
    
    rc.reset()
    rc.prefix = ''
    rc.padToSize = 10
    rc << 1 << 2
    assert [ '|1|2|_|_|_|_|_|_|_|_' ] == rc.merge()
  }
  
  @Test
  void testProcessRedundancy(){
    def rc = new RedundancyCollector( prefix:'X' )
    rc << 'a' << 'b' << [ 'c' ]
    rc.flush()
    assert """X|a|_|_
X|_|b|_
X|a|b|_
X|_|_|c
X|a|_|c
X|_|b|c
X|a|b|c
""" == rc.result.toString()

    rc.reset()
    rc.prefix = 'Z'
    rc << 'a'
    rc << [ 1, 2 ]
    rc << [ 'b', 'c', 'd' ]
    rc.flush()
    assert """Z|a|_|_
Z|_|1|_
Z|a|1|_
Z|_|_|b
Z|a|_|b
Z|_|1|b
Z|a|1|b
Z|_|2|_
Z|a|2|_
Z|_|2|b
Z|a|2|b
Z|_|_|c
Z|a|_|c
Z|_|1|c
Z|a|1|c
Z|_|2|c
Z|a|2|c
Z|_|_|d
Z|a|_|d
Z|_|1|d
Z|a|1|d
Z|_|2|d
Z|a|2|d
""" == rc.result.toString()

    rc.reset()
    rc.prefix = 'U'
    rc << 'a'
    rc << 'b'
    rc << [ '1', '2', '3' ]
    rc.flush()
    assert """U|a|_|_
U|_|b|_
U|a|b|_
U|_|_|1
U|a|_|1
U|_|b|1
U|a|b|1
U|_|_|2
U|a|_|2
U|_|b|2
U|a|b|2
U|_|_|3
U|a|_|3
U|_|b|3
U|a|b|3
""" == rc.result.toString()
  }
  
  @Test
  void testPadToSize(){
    def rc = new RedundancyCollector( prefix:'X', padToSize:10 )
    rc << 'a' << 'b' << [ 'c' ]
    rc.flush()
    assert """X|a|_|_|_|_|_|_|_|_|_
X|_|b|_|_|_|_|_|_|_|_
X|a|b|_|_|_|_|_|_|_|_
X|_|_|c|_|_|_|_|_|_|_
X|a|_|c|_|_|_|_|_|_|_
X|_|b|c|_|_|_|_|_|_|_
X|a|b|c|_|_|_|_|_|_|_
""" == rc.result.toString()
  }

}
