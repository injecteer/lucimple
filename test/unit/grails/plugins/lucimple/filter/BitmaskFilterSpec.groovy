package grails.plugins.lucimple.filter

import org.apache.lucene.analysis.core.WhitespaceAnalyzer
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.search.MatchAllDocsQuery
import org.apache.lucene.search.TopDocs
import org.apache.lucene.store.RAMDirectory

import grails.plugins.lucimple.IndexOpsService
import grails.plugins.lucimple.builder.ReusableDocumentBuilder
import spock.lang.Shared
import spock.lang.Specification

class BitmaskFilterSpec extends Specification {

  @Shared v = IndexOpsService.version
  @Shared RAMDirectory dir
  @Shared IndexSearcher s
  
  void setup(){
    dir = new RAMDirectory()
    IndexWriter w = new IndexWriter( dir, new IndexWriterConfig( v, new WhitespaceAnalyzer( v ) ) )
    ReusableDocumentBuilder b = new ReusableDocumentBuilder()
    [ [ 0b00000000, 0b00000000, 0b00111110, 0b11110000, 0b00000000, 0b00000000, 0b11111111, 0b11111111 ],
      [ 0b11111111, 0b11111100, 0b00111110, 0b11000000, 0b00000000, 0b01111110, 0b11111111, 0b00000000 ],
      null ].each{ bytes ->
      b{ bin bytes as byte[] }
      w.addDocument b.doc
      b.reset()
    }
    w.commit()
    s = new IndexSearcher( w.reader )
  }
  
	void "test lookup"() {
    expect:
    3 == s.indexReader.maxDoc()
    def f = new BitmaskFilter( 'bin', mask as byte[], 1 )
    TopDocs td = s.search new MatchAllDocsQuery(), f, 10
    td.totalHits == cnt
    
    where:
    cnt | mask                              
     0  | [ 0, 0, 0, 0, 0, 0, 0, 0 ]
     2  | [ 255, 255, 255, 255, 255, 255, 255, 255 ]
     1  | [ 255, 0, 0, 0, 0, 0, 0, 0 ]
     1  | [ 0, 0, 0, 0, 0, 0, 0, 255 ]
     2  | [ 0, 0, 0, 0b11000000, 0, 0, 0, 0 ]
     1  | [ 0, 0, 0, 0b00010000, 0, 0, 0, 0 ]
     0  | [ 0, 0, 0, 0b00000111, 0, 0, 0, 0 ]
     0  | [ 0 ]
     0  | [ 0, 0, 255, 255, 255, 255, 255, 255, 255, 255 ]
     0  | []
	}
  
  void "test strict lookup"() {
    expect:
    3 == s.indexReader.maxDoc()
    def f = new BitmaskFilter( 'bin', mask as byte[], 1, false )
    TopDocs td = s.search new MatchAllDocsQuery(), f, 10
    td.totalHits == cnt
  
    where:
    cnt | mask                              
    0   | [ 0, 0, 0, 0, 0, 0, 0, 0 ]
    0   | [ 255, 255, 255, 255, 255, 255, 255, 255 ]
    2   | [ 0, 0, 0, 0b11000000, 0, 0, 0, 0 ]
    1   | [ 0, 0, 0, 0b01110000, 0, 0, 0, 0 ]
    0   | [ 0, 0, 0, 0b01111000, 0, 0, 0, 0 ]
  }
  
  void 'test null masks'(){
    when:
    def f = new BitmaskFilter( 'bin', [ 0, 0, 0, 0b01110000, 0, 0, 0, 0 ] as byte[], 1, true )
    TopDocs td = s.search new MatchAllDocsQuery(), f, 10
    
    then:
    td.totalHits == 2
  }
}