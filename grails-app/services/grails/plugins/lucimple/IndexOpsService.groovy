package grails.plugins.lucimple

import static org.apache.lucene.util.Version.LUCENE_46 as VER
import grails.plugins.lucimple.analysis.DigitsAwareSimpleAnalyzer

import org.apache.lucene.analysis.core.*
import org.apache.lucene.analysis.de.GermanAnalyzer
import org.apache.lucene.analysis.fr.FrenchAnalyzer
import org.apache.lucene.analysis.it.ItalianAnalyzer
import org.apache.lucene.analysis.ru.RussianAnalyzer
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.lucene.analysis.util.CharArraySet
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.document.StringField
import org.apache.lucene.document.Field.Store
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.index.LogDocMergePolicy
import org.apache.lucene.index.Term
import org.apache.lucene.index.IndexWriterConfig.OpenMode
import org.apache.lucene.search.MatchAllDocsQuery
import org.apache.lucene.store.Directory
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.store.SimpleFSLockFactory
import org.apache.lucene.util.Version
import org.springframework.beans.factory.InitializingBean

class IndexOpsService implements InitializingBean {

  boolean transactional = false

  File indexRoot
  
  def grailsApplication
  
  static allDocsQuery = new MatchAllDocsQuery()
  
  static final Version version = VER
  
  def analyzers = [
           whitespace:{ new WhitespaceAnalyzer( VER ) },
           piped:{ new StopAnalyzer( VER, new CharArraySet( VER, [ '|' ], false ) ) },
           simple:{ new DigitsAwareSimpleAnalyzer( VER ) },
           simpleDefault:{ new SimpleAnalyzer( VER ) },
           keyword:{ new KeywordAnalyzer() },
           ru:{ new RussianAnalyzer( VER ) },
           uk:{ new RussianAnalyzer( VER ) },
           en:{ new StandardAnalyzer( VER ) },
           de:{ new GermanAnalyzer( VER ) },
           fr:{ new FrenchAnalyzer( VER ) },
           it:{ new ItalianAnalyzer( VER ) },
           ]
  
  Directory getDirectory( key ){
    File dir = new File( (File)indexRoot, key )
    FSDirectory.open dir, new SimpleFSLockFactory( dir ) 
  }
  
  @Override
  void afterPropertiesSet() throws Exception {
    if( !indexRoot.exists() ){
      if( indexRoot.mkdirs() ) log.info "indexRoot $indexRoot created"
      else throw new IOException( "indexRoot $indexRoot cannot be created!" )
    }
    if( grailsApplication.config.grails.lucimple.analyzers ) analyzers += grailsApplication.config.analyzers
  }
  
  def ensureValid( directory ){
    if( !DirectoryReader.indexExists( directory ) ){
      withIndexWriter( directory, true ){ iw -> 
        def empty = new Document()
        empty.add new StringField( 'fake', 'fake', Store.YES )
        iw.addDocument empty
      }
      withIndexWriter( directory, false ){ it.deleteDocuments new Term( 'fake', 'fake' ) }
    }
    directory
  }
  
  def withIndexWriterAndAnalyzer( analyzerKey, directory, create, action ){
    IndexWriter iw
    try{
      create = create || !DirectoryReader.indexExists( directory )
      def conf = new IndexWriterConfig( VER, analyzers[ analyzerKey ?: 'simple' ]() )
      conf.mergePolicy = new LogDocMergePolicy( mergeFactor:300 )
      conf.openMode = create ? OpenMode.CREATE : OpenMode.CREATE_OR_APPEND
      iw = new IndexWriter( directory, conf )
      action iw
      iw.commit()
    }catch( Exception e ){
      directory.clearLock directory.lockID
      throw e
    }finally{
      iw?.close()
    }
  }
    
  def analyzeField( analyzerKey, field, text ){
    def analyzer = analyzers[ analyzerKey ?: 'simple' ]()
    assert analyzer
    analyzer.tokenStream field, new StringReader( text.toString() )
  }

  def iterateTokens( analyzerKey, text, action = { termText -> } ){
    def tokenStream = analyzers[ analyzerKey ?: 'simple' ]().tokenStream( 'fake', new StringReader( text.toString() ) )
    while( tokenStream.incrementToken() ){
      action tokenStream.getAttribute( CharTermAttribute )
    }
    tokenStream.end()
    tokenStream.close()
  }
  
  def methodMissing( String name, args ){
    def closure
    if( name.startsWith( 'analyzeField' ) )
      closure = { field, text ->
        delegate.analyzeField( ( name - 'analyzeField' ).toLowerCase(), field, text )
      }
    else if( name.startsWith( 'withIndexWriter' ) )
      closure = { directory, create, action ->
        delegate.withIndexWriterAndAnalyzer( ( name - 'withIndexWriter' ).toLowerCase(), directory, create, action )
      }
    else if( name.startsWith( 'iterateTokens' ) )
      closure = { text, action ->
        delegate.iterateTokens( ( name - 'iterateTokens' ).toLowerCase(), text, action )
      }
    if( !closure ) throw new MissingMethodException( name, this, args )
    ExpandoMetaClass.enableGlobally()
    this.class.metaClass{
      "$name" closure
    }
    this."$name"( *args )
  }
  
}
