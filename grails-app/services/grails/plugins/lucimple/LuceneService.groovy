package grails.plugins.lucimple

import org.apache.lucene.store.*
import org.apache.lucene.search.*
import org.apache.lucene.document.*
import org.apache.lucene.index.*
import org.apache.lucene.index.IndexWriterConfig.OpenMode

import grails.plugins.lucimple.builder.*

import org.springframework.beans.factory.InitializingBean
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

abstract class LuceneService extends LuceneSupport implements InitializingBean {

  def clazz
    
  def directory
  
  def messageSource
  
  private SearcherManager searcherManager

  IndexWriter writer
  
  MergeScheduler mergeScheduler = new ConcurrentMergeScheduler( maxThreadCount:1, maxMergeCount:1 )
  
  def fieldsRemovalRegexp
  
  @Override
  void afterPropertiesSet() throws Exception {
    if( RAMDirectory != directory?.class ){
      if( !indexKey && clazz ) indexKey = clazz.name.substring( clazz.name.lastIndexOf( '.' ) + 1 ).toLowerCase()
      if( !indexKey ) throw new IllegalArgumentException( "${this}: indexKey must not be empty!" )
      directory = indexOpsService.ensureValid( indexOpsService.getDirectory( indexKey ) )
    }    
    initWriter()
  }
  
  void initWriter(){
    long start = System.currentTimeMillis()
    writer?.close()
    def conf = new IndexWriterConfig( indexOpsService.version, indexOpsService.analyzers.whitespace() )
    if( RAMDirectory == directory?.class )
      conf.openMode = OpenMode.CREATE
    else{
      if( IndexWriter.isLocked( directory ) ){
        IndexWriter.unlock directory
        directory.clearLock directory.lockID
      }
      conf.mergeScheduler = mergeScheduler ?: new NoMergeScheduler()
      conf.mergePolicy = new LogDocMergePolicy()
      conf.openMode = OpenMode.CREATE_OR_APPEND
      conf.writeLockTimeout = 30000l
    }
    
    writer = new IndexWriter( directory, conf )
    searcherManager = new SearcherManager( writer, true, new SearcherFactory() )
  }
  
  def withSearcher( action ){
    IndexSearcher s = searcherManager.acquire()
    try{
      return action( s )
    }finally{
      searcherManager.release s
    }
  }

  def withWriterContext( Closure action ){
    withWriterContext true, action
  }
  
  def withWriterContext( boolean reopenNeeded, Closure action ){
    def builder = action.parameterTypes.size() ? new ReusableDocumentBuilder( indexOpsService:indexOpsService, fieldsRemovalRegexp:fieldsRemovalRegexp ) : null
    try{
      switch( action.maximumNumberOfParameters ){
        case 0: action(); break
        case 1: action( builder ); break
        case 2: action builder, writer
      }
      writer.commit()
      if( reopenNeeded ) reopen()
    }catch( Exception e ){
      log.error '', e
      writer.rollback()
      initWriter()
    }
  }
  
  int countFor( Query query, Filter filter ){
    withSearcher{ IndexSearcher s ->
      TotalHitCountCollector c = new TotalHitCountCollector()
      s.search query ?: indexOpsService.allDocsQuery, filter, c
      c.totalHits
    }
  }
  
  def minus( obj ){ delete obj }
  
  def delete( obj ){
    withWriterContext{-> writer.deleteDocuments new Term( idField, obj[ idField ].toString() ) }
  }
  
  def merge(){
    withWriterContext{-> writer.forceMergeDeletes() }
  }
  
  def replace( oldObj, newObj ){
    withWriterContext{ builder ->
      writer.deleteDocuments new Term( idField, oldObj[ idField ].toString() )
      this.indexSingle newObj, builder, writer, false
    }
  }
  
  @Transactional( readOnly = true, propagation = Propagation.SUPPORTS )
  def leftShift( obj ){ 
    index obj
    this
  } 
  
  @Transactional( readOnly = true, propagation = Propagation.SUPPORTS )
  def index( obj ){
    if( !Collection.isAssignableFrom( obj.getClass() ) ) obj = [ obj ]
    withWriterContext{ builder -> 
      for( o in obj ) try{
        this.indexSingle o, builder, writer
      }catch( Exception e ){ 
        log.error 'index failed', e 
      }
    }
  }

  def reopen(){
    long start = System.currentTimeMillis()
    searcherManager.maybeRefresh()
    log.info "re-openned in ${System.currentTimeMillis() - start} ms"
  }
  
  def get( id ){
    withSearcher{ get it, id }
  }
  
  int count( Query q ){
    withSearcher{ count it, q }
  }
  
  int remove( Closure query ){
    int count
    withWriterContext{->
      withSearcher{ searcher ->
        count = remove( writer, searcher, query )
      }
    }
    count
  }
  
  def asResult( Closure binder, Map params, Closure query ){
    withSearcher{ asResult it, toQuery( query ), null, params, binder }
  }
  
  def asResult( Query query, Filter filter, Map params, Closure binder = { doc, docNum -> } ){
    withSearcher{ asResult it, query, filter, params, binder }
  }
  
  def find( Closure binder, Closure query ){
    def obj
    eachQuerriedDoc( toQuery( query ), [ max:1 ] ){ Document doc, int ix ->
      obj = binder( doc, ix )
    }  
    obj
  }
    
  def eachDoc( Query query, Filter filter, Map params, Closure action ){
    withSearcher{ eachDoc it, query, filter, params, action }
  }
  
  def eachFilteredDoc( Filter filter, Map params, Closure action ){
    eachDoc indexOpsService.allDocsQuery, filter, params, action
  }
  
  def eachQuerriedDoc( Query query, Map params, Closure action ){
    eachDoc query, null, params, action
  }
  
  def eachDoc( Map params, Closure action ){
    eachDoc indexOpsService.allDocsQuery, null, params, action
  }
  
  def eachTerm( String field, Closure action = { String term, int freq, int ix -> } ){
    withSearcher{ eachTerm it.indexReader, field, action }
  }
  
  def eachTerm( String field, String prefix, Closure action = { String term, int freq, int ix -> } ){
    withSearcher{ eachTerm it.indexReader, field, prefix, action }
  }
  
  def eachPrefixedTerm( String field, String prefix, String separator, Closure action ){
    withSearcher{ eachPrefixedTerm it.indexReader, field, prefix, separator, action }
  }
  
  def eachDocGroup( String field, Closure action = { String term, docs -> } ){
    withSearcher{ eachDocGroup it.indexReader, field, action }
  }
}