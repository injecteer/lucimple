grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir	= "target/test-reports"
//grails.project.war.file = "target/${appName}-${appVersion}.war"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits( "global" ) {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
      grailsHome()        // uncomment the below to enable remote dependency resolution
      // from public Maven repositories
      grailsCentral()
      mavenCentral()
//      mavenRepo "http://snapshots.repository.codehaus.org"
//      mavenRepo "http://repository.codehaus.org"
      //mavenRepo "http://download.java.net/maven/2/"
      //mavenRepo "http://repository.jboss.com/maven2/
    }
    
    def luceneVer = '4.6.0'
    
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
      compile( 
          "org.apache.lucene:lucene-core:$luceneVer",
          "org.apache.lucene:lucene-analyzers-common:$luceneVer",
          "org.apache.lucene:lucene-queries:$luceneVer",
          "org.apache.lucene:lucene-queryparser:$luceneVer",
      )
    }
    
    plugins {
    }

}
