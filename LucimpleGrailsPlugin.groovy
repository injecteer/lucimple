import grails.util.Environment

class LucimpleGrailsPlugin {
    // the plugin version
    def version = "0.2"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "1.3 > *"
    // the other plugins this plugin depends on
//    def dependsOn = [:]
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views",
            "grails-app/domain",
            "grails-app/i18n",
            "web-app"
    ]

    // TODO Fill in these fields
    def author = "Konstantyn Smirnov"
    def authorEmail = "injecteer@gmail.com"
    def title = "Lucimple Plugin"
    def description = '''The plugin provides a light-weight wrapper for Lucene 3.* API.
No need to create any sort of mapping files, like in Compass.  
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/lucimple"

    def watchedResources = [
//                            "file:./grails-app/services/**/*.groovy",
                            ]
  
      
    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before 
    }

    def doWithSpring = {
      def appName = application.metadata.'app.name'
      String base = Environment.current == Environment.DEVELOPMENT ? System.getProperty( 'user.home' ) : System.getProperty( 'catalina.home' )
      indexRoot File, application.config.grails.lucimple.indexRoot ?: "$base/index/$appName"
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }
}
