package grails.plugins.lucimple.filter;

import java.io.IOException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.Filter;
import org.apache.lucene.util.Bits;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.FixedBitSet;
public class BitmaskFilter extends Filter {
  
  private BitSet checkMask;
  
  private String binField;

  private boolean nullMasksAllowed = false;
  
  private Set<String> bindFieldSet = new HashSet<String>();

  private int minCommonCardinality = 1;
  
  public BitmaskFilter( String binField, byte[] mask, int minCommonCardinality ){
    this.setBinField( binField );
    this.setBitmask( mask );
    this.minCommonCardinality = minCommonCardinality;
  }
  
  public BitmaskFilter( String binField, BitSet mask, int minCommonCardinality ){
    this.setBinField( binField );
    this.checkMask = mask;
    this.minCommonCardinality = minCommonCardinality;
  }
  
  /**
   * 
   * @param binField - field name
   * @param mask - mask to match
   * @param minCommonCardinality - the lowest cardinality value produced by and-ing
   */
  public BitmaskFilter( String binField, byte[] mask, int minCommonCardinality, boolean nullMasksAllowed ){
    this.setBinField( binField );
    this.setBitmask( mask );
    this.minCommonCardinality = minCommonCardinality;
    this.nullMasksAllowed = nullMasksAllowed;
  }
  
  public String getBinField() {
    return binField;
  }

  public void setBinField( String binField ) {
    this.binField = binField;
    bindFieldSet.clear();
    bindFieldSet.add( binField );
  }

  void setBitmask( byte[] bitmask ) {
    checkMask = BitSet.valueOf( bitmask );
  }

  @Override
  public DocIdSet getDocIdSet( AtomicReaderContext context, Bits acceptDocs ) throws IOException {
    AtomicReader reader = context.reader();
    int size = reader.maxDoc();
    if( 0 == size || null == checkMask || 0 == checkMask.cardinality() || null == binField ) return null;
    FixedBitSet result = new FixedBitSet( size );

    for( int ix = 0; ix < size; ix++ ){
      if( null != acceptDocs && !acceptDocs.get( ix ) ) continue;
      BytesRef maskBR = reader.document( ix, bindFieldSet ).getBinaryValue( binField );
      if( null == maskBR || 0 == maskBR.length ){
        if( nullMasksAllowed ) result.set( ix );
        continue;
      }
      BitSet maskBS = BitSet.valueOf( maskBR.bytes );
      if( 0 == maskBS.cardinality() ) continue;
      maskBS.and( checkMask );
      if( minCommonCardinality <= maskBS.cardinality() ) result.set( ix );
    }
    return result;
  }
  
  public BitSet getCheckMask() {
    return checkMask;
  }
  
  public int getMinCommonCardinality() {
    return minCommonCardinality;
  }
  
  public void setMinCommonCardinality( int minCommonCardinality ) {
    this.minCommonCardinality = minCommonCardinality;
  }
  
  public boolean isNullMasksAllowed() {
    return nullMasksAllowed;
  }
  
  @Override
  public String toString() {
    return "<bitMask:" + binField + " card=" + checkMask.cardinality() + ">";
  }

}