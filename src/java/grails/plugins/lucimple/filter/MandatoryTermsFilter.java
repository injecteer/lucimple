package grails.plugins.lucimple.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.DocsEnum;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.Filter;
import org.apache.lucene.util.Bits;
import org.apache.lucene.util.OpenBitSet;
public class MandatoryTermsFilter extends Filter {
  
  private List<Term> terms = new ArrayList<Term>();
  
  public List<Term> getTerms() {
    return terms;
  }

  public void setTerms( List<Term> terms ) {
    this.terms = terms;
  }
  
  public void setTerms( Term term ) {
    this.terms.add( term );
  }

  @Override
  public DocIdSet getDocIdSet( AtomicReaderContext context, Bits acceptDocs ) throws IOException {
    AtomicReader reader = context.reader();
    int size = reader.maxDoc();
    Bits liveDocs = MultiFields.getLiveDocs( reader );
    if( 0 == size || terms.isEmpty() || null == liveDocs ) return null;
    
    OpenBitSet result = new OpenBitSet( size );
    result.set( 0, size );
    DocsEnum td;
    int pos, prevPos;
    
    for( Term term : terms ){
      if( 0 == term.text().length() ) continue;
      td = reader.termDocsEnum( term );
      prevPos = 0;
      while( ( pos = td.nextDoc() ) != DocsEnum.NO_MORE_DOCS ){
        result.clear( prevPos, pos );
        if( liveDocs.get( pos ) ) result.clear( pos );
        prevPos = pos + 1;
      }
      result.clear( prevPos, size );
//      System.out.println( term + " -> " + result.cardinality() );
    }
    return result;
  }
  
  public String toString(){
    StringBuilder sb = new StringBuilder();
    for( Term term:terms ) sb.append( " +" ).append( term.toString() );
    return "<" + sb.substring( 1 ) + ">"; 
  }
  
}