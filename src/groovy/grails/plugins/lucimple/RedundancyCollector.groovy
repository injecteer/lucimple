package grails.plugins.lucimple

class RedundancyCollector {
  
  Set uniqueCache = new HashSet()
  
  StringBuilder result = new StringBuilder()
  
  String separator = '|'
  
  String empty = '_'
  
  String prefix = ''
  
  def data = []
  
  boolean clean = false
  
  boolean hashLines = false
  
  boolean allowAllEmpty = false
  
  int padToSize = 0
  
  def leftShift( elem ){ add elem }
  
  /**
   * converts the this.list according to:
   * 
   * [ 1, 2 ] [ a, b ] -> 1a 1b 2a 2b 
   * [ 1, 2, 3 ] [ a, b, c ] -> 1a 1b 1c 2a 2b 2c 3a 3b 3c
   * [ 1, 2 ] [ a, b, c ] -> 1a 1b 1c 2a 2b 2c
   * [ 1, 2 ] [ a, b, c ], F -> 1aF 1bF 1cF 2aF 2bF 2cF
   * 
   * @return the list containing all combinations of lists
   */
  def add( elem ){
    if( !data ) data << []
    
    if( elem && isListable( elem ) ){
      if( 1 == elem.size() ){
        def e = elem.iterator().next()
        data*.add( clean ? cleanUp( e ) : e )

      }else{
        def clones = []
        elem.each{ e ->
          clones.addAll data.collect{ it + ( clean ? cleanUp( e ) : e ) }
        }
        data.clear()
        data.addAll clones 
      }
      
    }else
      data*.add( elem && clean ? cleanUp( elem ) : elem )
      
    this
  }
  
  boolean asBoolean(){ 0 != result.length() } 
  
  void combine(){
    def curr = new StringBuilder()
    for( list in data ){
      ( ( allowAllEmpty ? 0 : 1 )..<( 1 << list.size() ) ).each{ mask ->
        curr.length = 0
        boolean allEmpty = !allowAllEmpty
        list.eachWithIndex{ val, ix ->
          curr << separator
          if( mask & ( 1 << ix ) && val ){
            allEmpty = false  
            curr << val 
          }else
            curr << empty          
        }
        if( !allEmpty ){
          if( list.size() < padToSize ) ( padToSize - list.size() ).times{ curr << separator << empty }
          curr.insert 0, prefix
          int hash = curr.toString().hashCode()
          if( uniqueCache.add( hash ) ) result << ( hashLines ? hash : curr ) << '\n'
        }
      }
    }
  }
  
  static boolean isListable( v ){ 
    v && ( v.class.array || Collection.isAssignableFrom( v.class ) )
  }
  
  /**
   * spawnMultiple and combine
   * 
   */
  void flush(){
    combine()
    data.clear()
  }
  
  List merge(){
    def sb = new StringBuilder()
    data.collect{ list -> 
      sb.length = 0
      sb << prefix 
      for( elem in list ){
        sb << separator << ( elem ? elem : empty )
      }
      if( list.size() < padToSize ) ( padToSize - list.size() ).times{ sb << separator << empty }
      sb.toString()
    }
  }

  // for tests
  void reset(){
    result.length = 0
    uniqueCache.clear()
    data.clear()
  }
  
  static String cleanUp( val ){
    val?.toString()?.replaceAll( /[^\p{L}\d_]/, '' )
  }
  
}
