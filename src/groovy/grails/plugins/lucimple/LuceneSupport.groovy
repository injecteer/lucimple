package grails.plugins.lucimple

import org.apache.lucene.store.*
import org.apache.lucene.search.*
import org.apache.lucene.util.BytesRef
import org.apache.lucene.document.*
import org.apache.lucene.index.*
import org.apache.lucene.index.IndexWriterConfig.OpenMode
import org.apache.lucene.index.TermsEnum.SeekStatus
import org.apache.lucene.queries.TermsFilter as TF

import grails.plugins.lucimple.builder.QueryBuilder
import grails.plugins.lucimple.builder.ReusableDocumentBuilder

abstract class LuceneSupport {

  String idField = 'id'
  
  String indexKey
    
  IndexOpsService indexOpsService

  abstract void toDocument( obj, ReusableDocumentBuilder builder )
  
  def indexSingle( obj, ReusableDocumentBuilder builder, iw, boolean update = true ){
    try{
      if( !obj ) return
      this.toDocument obj, builder
      def doc = builder.doc
      if( !doc.fields ) return
      Term id = idField ? new Term( idField, obj[ idField ].toString() ) : null
      if( IndexWriter == iw.class )
        ( update && id ) ? iw.updateDocument( id, doc ) : iw.addDocument( doc )
      else
        ( update && id ) ? iw*.updateDocument( id, doc ) : iw*.addDocument( doc )
    }catch( Exception e ){
      log.error "index failed: $obj", e
      throw e
    }finally{
      builder.reset()
    }
  } 
  
  def get( IndexSearcher searcher, id ){
    def res = searcher.search( new TermQuery( new Term( idField, id.toString() ) ), 1 )
    res.totalHits ? searcher.doc( res.scoreDocs[ 0 ].doc ) : null
  }
  
  int count( IndexSearcher searcher, Query q ){
    searcher.search( q, 1 ).totalHits
  }
  
  def asResult( IndexSearcher searcher, Query query, Filter filter, Map params, Closure binder = { doc, docIx -> } ){
    def results = [] 
    int total = eachDoc( searcher, query, filter, params ){ doc, docIx ->
      def obj = binder( doc, docIx )
      if( obj ) results << obj   
    }
    [ total:total, list:results, filter:filter ]
  }
  
  def eachDoc( IndexSearcher searcher, Query query, Filter filter, Map params, Closure action = { doc, docIx -> } ){
    if( !query ) query = indexOpsService.allDocsQuery
    params.max = params.max ? params.max.toInteger() : 10
    params.offset = params.offset ? params.offset.toInteger() : 0
    
    def args = [ query, filter, params.offset + params.max ]
    if( params.sort ) args << params.sort
    def topDocs = searcher.search( *args )
    
    if( topDocs.totalHits ){
      int upper = Math.min( params.offset + params.max, topDocs.totalHits )
      topDocs.scoreDocs[ params.offset..<upper ].each{ 
        action searcher.doc( it.doc, params.fieldSelector ), it.doc
      }
    }
    topDocs.totalHits
  }

  /**
   * Passes the unique values to the closure, based on the following rules:
   *
   * given the values:
   * aaa_bbb_ccc
   * aaa_bbb_ddd
   * aaa_fff_eee
   * ggg_hhh_kkk
   *
   * prefix == '' returns:
   * aaa:3, ggg:1
   *
   * prefix == 'aaa' returns:
   * aaa_bbb:2, aaa_fff:1
   *
   * prefix == 'aaa_bbb' returns:
   * aaa_bbb_ccc:1, aaa_bbb_ddd:1
   *
   * @param indexReader
   * @param field
   * @param prefix
   * @param separator
   * @param action
   */
  static eachPrefixedTerm( IndexReader reader, String field, String prefix, String separator, Closure action ){
    Fields fields = MultiFields.getFields( reader )
    if( !fields ) return
    TermsEnum te = fields.terms( field )?.iterator( null )
    if( !te || SeekStatus.END == te.seekCeil( new BytesRef( prefix ) ) ) return
    String lastText, text = te.term().utf8ToString()
    int ix = 0, pos = 0, totalFreq = 0
    while( text?.startsWith( prefix ) ){
      if( -1 != ( pos = text.indexOf( separator, prefix.size() + separator.size() ) ) ) text = text.substring( 0, pos )
      if( lastText == text ) 
        totalFreq += te.docFreq()
      else{
        if( lastText && lastText != prefix ) action lastText, totalFreq, ix++
        lastText = text
        totalFreq = te.docFreq()
      }
      text = te.next()?.utf8ToString()
    }
    if( lastText && lastText != prefix ) action lastText, totalFreq, ix++
  }
  
  static eachTerm( IndexReader reader, String field, Closure action = { String term, int freq, int ix -> } ){
    eachTerm reader, field, null, action
  }
  
  static eachTerm( IndexReader reader, String field, String prefix, Closure action = { String term, int freq, int ix = 0 -> } ){
    Fields fields = MultiFields.getFields( reader )
    if( !fields ) return
    TermsEnum te = fields.terms( field )?.iterator( null )
    if( !te ) return
    int ix = 0
    String text
    if( prefix ){
      //seek
      prefix = prefix.toLowerCase()
      while( null != ( text = te.next()?.utf8ToString() ) && !text.toLowerCase().startsWith( prefix ) ){}
      if( 3 == action.parameterTypes.size() ) action text, te.docFreq(), ix++
      else if( 2 == action.parameterTypes.size() ) action text, te.docFreq()
    }
    while( null != ( text = te.next()?.utf8ToString() ) && ( null == prefix || text.toLowerCase().startsWith( prefix ) ) ){
      if( 3 == action.parameterTypes.size() ) action text, te.docFreq(), ix++
      else if( 2 == action.parameterTypes.size() ) action text, te.docFreq()
    }
  }
  
  static eachDocGroup( IndexReader reader, String field, Closure action = { String term, docs -> } ){
    Fields fields = MultiFields.getFields( reader )
    if( !fields ) return
    TermsEnum te = fields.terms( field )?.iterator( null )
    if( !te ) return
    Bits liveDocs = MultiFields.getLiveDocs( reader )
    String text
    int docId
    while( null != ( text = te.next()?.utf8ToString() ) ){
      if( 2 > te.docFreq() ) continue
      DocsEnum docsEnum = null
      docsEnum = te.docs( liveDocs, docsEnum, DocsEnum.FLAG_NONE )
      docId = 0
      def docs = []
      while( DocIdSetIterator.NO_MORE_DOCS != ( docId = docsEnum.nextDoc() ) ) docs << reader.document( docId ) 
      action text, docs
    }
  }
  
  def bindPrimitiveFields( obj, Document doc ) {
    if( !doc ) return null
    if( Class == obj.class ) obj = obj.getConstructor().newInstance()
    for( Field field in doc.fields ){ 
      def prop = obj.class.metaClass.getMetaProperty( field.name() )
      if( !prop ) continue
      def val
      switch( prop.type ){
        case String: val = field.stringValue(); break
        case [ int, Integer ]: val = ( null == field.numericValue() ? field.stringValue() : field.numericValue() ).toInteger(); break
        case [ long, Long ]: val = ( null == field.numericValue() ? field.stringValue() : field.numericValue() ).toLong(); break
        case [ float, Float ]: val = ( null == field.numericValue() ? field.stringValue() : field.numericValue() ).toFloat(); break
        case [ double, Double ]: val = ( null == field.numericValue() ? field.stringValue() : field.numericValue() ).toDouble(); break

        case BitSet: val = BitSet.valueOf field.binaryValue().bytes; break
        case Date: val = this.dateBinder( field.stringValue() ); break
        case List: val = doc.getValues( field.name() ) as List; break
        case TreeSet: val = doc.getValues( field.name() ) as TreeSet; break
        case Set: val = doc.getValues( field.name() ) as Set; break
      } 
      if( null != val ) prop.setProperty obj, val
    }
    obj
  }
  
  Date dateBinder( String val ){
    DateTools.stringToDate val
  }
  
  int remove( IndexWriter writer, IndexSearcher searcher, Closure query ){
    Query q = toQuery( query )
    int count = count( searcher, q )
    if( count ) writer.deleteDocuments q
    count
  }

  static void serializeRAMDirectory( RAMDirectory dir, output ){
    if( null == dir ) return
    output?.withObjectOutputStream{ out ->
      out.writeLong dir.sizeInBytes()
      out.writeInt dir.fileMap.size()
      dir.fileMap.each{ String name, RAMFile rf ->
        out.writeObject name
        out.writeLong rf.length
        out.writeLong rf.sizeInBytes
        out.writeObject rf.buffers
      }
    }
  }
  
  static RAMDirectory deserializeRAMDirectory( input ){
    RAMDirectory rd = new RAMDirectory()
    input?.withObjectInputStream{ inp ->
      rd.sizeInBytes.set inp.readLong()
      int size = inp.readInt()
      for( int ix = 0; ix < size; ix++ ){
        RAMFile rf = rd.newRAMFile()
        String name = inp.readObject()
        rf.length = inp.readLong()
        rf.sizeInBytes = inp.readLong()
        rf.buffers = inp.readObject()
        rd.fileMap[ name ] = rf
      }
    }
    rd.fileMap ? rd : null
  }
  
	Query toQuery( Closure query ){
		QueryBuilder qb = new QueryBuilder()
		query.delegate = qb
		query.call()
		qb.root
	}
  
}