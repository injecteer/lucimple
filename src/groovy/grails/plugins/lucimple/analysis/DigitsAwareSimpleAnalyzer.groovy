package grails.plugins.lucimple.analysis

import grails.plugins.lucimple.IndexOpsService

import org.apache.lucene.analysis.Analyzer.TokenStreamComponents
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.Tokenizer
import org.apache.lucene.util.Version

public class DigitsAwareSimpleAnalyzer extends Analyzer { 

  private Version matchVersion = IndexOpsService.version
  
  DigitsAwareSimpleAnalyzer(){}
  
  DigitsAwareSimpleAnalyzer( Version matchVersion ){
    this.matchVersion = matchVersion
  }
  
  @Override
  TokenStreamComponents createComponents( String fieldName, Reader reader ) {
    new TokenStreamComponents( new DigitsAwareLowerCaseTokenizer( matchVersion, reader ) )
  }
  
}