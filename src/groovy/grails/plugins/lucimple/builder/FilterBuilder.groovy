package grails.plugins.lucimple.builder

import grails.plugins.lucimple.filter.MandatoryTermsFilter as MTF

import org.apache.lucene.document.DateTools
import org.apache.lucene.document.DateTools.Resolution
import org.apache.lucene.index.Term
import org.apache.lucene.queries.BooleanFilter
import org.apache.lucene.queries.FilterClause
import org.apache.lucene.queries.TermsFilter
import org.apache.lucene.search.*
import org.apache.lucene.search.BooleanClause.Occur
import static org.apache.lucene.search.BooleanClause.Occur.*
import org.apache.lucene.util.BytesRef
import org.apache.lucene.util.NumericUtils

class FilterBuilder extends BuilderSupport {

  static final numericRanges = [ integer:'newIntRange', int:'newIntRange', long:'newLongRange', float:'newFloatRange', double:'newDoubleRange' ]
  
  BooleanFilter root = new BooleanFilter()
  
  Occur defaultOccur

  Stack nodeChain, occurChain
  
  Resolution defaultDateResolution = Resolution.DAY
  
  static occurMap = [ or:SHOULD, and:MUST, not:MUST_NOT ]
  
  FilterBuilder( Occur defaultOccur = null ){
    this.defaultOccur = defaultOccur ?: MUST
    occurChain = [ this.defaultOccur ] as Stack
    nodeChain = [ root ] as Stack
  }
  
  @Override
  Object createNode( Object name ) {
    if( 'call' == name ) return root
    BooleanFilter curr = nodeChain.peek()
    if( 'group' == name ){
      def newF = new BooleanFilter()
      curr.add new FilterClause( newF, occurChain.peek() )
      curr = newF
      nodeChain << newF
      occurChain << defaultOccur
    }else if( occurMap.containsKey( name ) )
      occurChain << occurMap[ name ]
    name
  }

  @Override
  Object createNode( Object name, Object value ){
    if( null == value ) return name
    BooleanFilter curr = nodeChain.peek()
    Occur occur = occurChain.peek()
    if( 'filter' == name && Filter.isAssignableFrom( value.class ) )
      curr.add new FilterClause( value, occur )
    else if( 'query' == name && Query.isAssignableFrom( value.class ) )
      curr.add new FilterClause( new QueryWrapperFilter( value ), occur )
     
    else 
      curr.add new FilterClause( newTF( asTerms( [], name, value ), SHOULD ), occur )
    name
  }

  /**
   * 
   * Special queries like range or prefix
   * 
   */
  @Override
  Object createNode( Object name, Map attrs ){
    BooleanFilter curr = nodeChain.peek()
    Occur occur = occurChain.peek()        
    def from = attrs.from ?: attrs.value
    def to = attrs.to ?: attrs.value
    
    switch( name ){
      case [ 'all', 'any' ]:
        def occ = 'all' == name ? MUST : SHOULD
        def terms = []
        attrs.each asTerms.curry( terms )
        curr.add new FilterClause( newTF( terms, occ ), occur )
        break
        
      case 'prefixed':
        attrs.each{ k, v -> if( v ) curr.add new FilterClause( new PrefixFilter( new Term( k, v.toString() ) ), occur ) }
        break
      
      case 'dateRange':
        Resolution res
        switch( attrs.resolution ){
          case String: res = attrs.resolution.toUpperCase() as Resolution; break
          case Resolution: res = attrs.resolution; break
          default: res = defaultDateResolution
        } 
        from = DateTools.dateToString( from, res )
        to = DateTools.dateToString( to, res )
        
      case 'range':
        curr.add new FilterClause( new TermRangeFilter( attrs.field, new BytesRef( from ), new BytesRef( to ), true, true ), occur )
        break
        
      case ~/^numeric.*/: 
        int precisionStep = null != attrs.precisionStep ? attrs.precisionStep.toInteger() : NumericUtils.PRECISION_STEP_DEFAULT
        String type = name.substring( 7 ).toLowerCase() ?: 'int'
        if( numericRanges.containsKey( type ) )
          curr.add new FilterClause( NumericRangeFilter."${numericRanges[ type ]}"( attrs.field, precisionStep, from, to, true, true ), occur )
        break
    }
    name
  }

  @Override
  Object createNode( Object name, Map attrs, Object closure ){
    name
  }

  @Override
  void setParent( Object parent, Object child ) {
  }

  void nodeCompleted( Object parent, Object node ){
    if( 'group' == node ){
      nodeChain.pop()
      occurChain.pop()
    }else if( occurMap.containsKey( node ) )
      occurChain.pop()
  }
  
  private asTerms = { collector, field, value ->
    Collection.isAssignableFrom( value.getClass() ) ? 
      value.each{ if( it ) collector << new Term( field, it.toString() ) } :
      collector << new Term( field, value.toString() )
    collector
  }
  
  static newTF( terms, occur ){
    if( !terms ) return null
    if( Term == terms.getClass() ) occur = SHOULD
    switch( occur ){
      case SHOULD: return new TermsFilter( terms )
      case MUST: return new MTF( terms:terms )
    }
    null
  }

}
