package grails.plugins.lucimple.builder

import java.util.Stack;

import org.apache.lucene.search.BooleanClause.Occur
import org.apache.lucene.document.DateTools
import org.apache.lucene.document.DateTools.Resolution
import org.apache.lucene.index.Term
import static org.apache.lucene.search.BooleanClause.Occur.*
import org.apache.lucene.search.*
import org.apache.lucene.util.BytesRef

import groovy.util.BuilderSupport

class QueryBuilder extends BuilderSupport {

  BooleanQuery root = new BooleanQuery()
  
  Occur defaultOccur

  Stack nodeChain, occurChain
  
  Resolution defaultDateResolution = Resolution.DAY
  
  static occurMap = [ or:SHOULD, and:MUST, not:MUST_NOT ]
 
  QueryBuilder( Occur defaultOccur = Occur.MUST ){
    this.defaultOccur = defaultOccur ?: MUST
    occurChain = [ this.defaultOccur ] as Stack
    nodeChain = [ root ] as Stack
  }
  
  @Override
  Object createNode( Object name ) {
    if( 'call' == name ) return root
    BooleanQuery curr = nodeChain.peek()
    if( 'group' == name ){
      def newQ = new BooleanQuery()
      curr.add new BooleanClause( newQ, occurChain.peek() )
      curr = newQ
      nodeChain << newQ
      occurChain << defaultOccur
    }else if( occurMap.containsKey( name ) )
      occurChain << occurMap[ name ]
    name
  }

  @Override
  Object createNode( Object name, Object value ){
    if( null == value ) return name
    BooleanQuery curr = nodeChain.peek()
    Occur occur = occurChain.peek()

    if( 'query' == name && Query.isAssignableFrom( value.class ) )
      curr.add new BooleanClause( value, occur )
     
    else newTQ( curr, Collection.isAssignableFrom( value.class ) ?
         value.findAll{ it }.collect{ new Term( name, it.toString() ) } :
         new Term( name, value.toString() ), occur )
    name
  }

  /**
   * 
   * Special queries like range or prefix
   * 
   */
  @Override
  Object createNode( Object name, Map attrs ){
    Occur occur = occurChain.peek()
    BooleanQuery curr = nodeChain.peek()
    
    def from = attrs.from ?: attrs.value
    def to = attrs.to ?: attrs.value
    
    switch( name ){
      case [ 'all', 'any' ]:
        Occur occ = 'all' == name ? Occur.MUST : Occur.SHOULD
        BooleanQuery q = new BooleanQuery()
        attrs.each{ field, value ->
          newTQ( q, Collection.isAssignableFrom( value.class ) ?
            value.findAll{ it }.collect{ new Term( field, it.toString() ) } :
            new Term( field, value.toString() ), occ )
        }
        curr.add q, occur
        break
        
      case 'prefixed':
        attrs.each{ k, v -> if( v ) curr.add new BooleanClause( new PrefixQuery( new Term( k, v.toString() ) ), occur ) }
        break
        
      case 'dateRange':
        Resolution res
        switch( attrs.resolution ){
          case String: res = attrs.resolution.toUpperCase() as Resolution; break
          case Resolution: res = attrs.resolution; break
          default: res = defaultDateResolution
        }
        from = DateTools.dateToString( from, res )
        to = DateTools.dateToString( to, res )
        
      case 'range':
        curr.add new BooleanClause( new TermRangeQuery( attrs.field, new BytesRef( from ), new BytesRef( to ), true, true ), occur )
        break
        
      case ~/^numeric.*/: 
        String type = name.substring( 7 ).toLowerCase() ?: 'int'
        if( FilterBuilder.numericRanges.containsKey( type ) )
          curr.add new BooleanClause( NumericRangeQuery."${FilterBuilder.numericRanges[ type ]}"( attrs.field, from, to, true, true ), occur )
    }
    name
  }

  @Override
  Object createNode( Object name, Map attrs, Object closure ){
    name
  }

  @Override
  void setParent( Object parent, Object child ) {
  }

  void nodeCompleted( Object parent, Object node ){
    if( 'group' == node ){
      nodeChain.pop()
      occurChain.pop()
    }else if( occurMap.containsKey( node ) )
      occurChain.pop()
  }
 
  static newTQ( Query q, terms, occur = Occur.SHOULD ){
    if( !terms ) return null
    for( t in terms ) q.add new BooleanClause( new TermQuery( t ), occur )
  }

}