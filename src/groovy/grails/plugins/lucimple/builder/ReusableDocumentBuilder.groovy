package grails.plugins.lucimple.builder

import java.util.regex.Pattern

import grails.plugins.lucimple.IndexOpsService

import org.apache.lucene.document.*
import org.apache.lucene.document.DateTools.Resolution
import org.apache.lucene.document.Field.Store

/**
 * The Document instance with it's fields is re-used in order to keep the memory finger-print at minimum.
 * 
 * The fields are accumulated
 * 
 * @author Injecteer
 *
 */
class ReusableDocumentBuilder extends BuilderSupport {

  static final byte[] EMPTY_BINARY = [] as byte[]
  
  IndexOpsService indexOpsService = new IndexOpsService()
  
  FieldFactory fieldFactory = new FieldFactory()
  
  Document doc = new Document() 

  Store defaultStore = Store.YES
  
  Boolean defaultIndex = true
  
  Resolution defaultDateResolution = Resolution.DAY

  Number defaultNumericValue = 0
  
  Set<String> numericFieldsJustAdded = new HashSet<String>()
  
  def fieldsRemovalRegexp

  void setDefaultStore( boolean v ){
    defaultStore = v ? Store.YES : Store.NO
  }
    
  @Override
  Object createNode( Object name ){
    if( !fieldFactory.indexOpsService ) fieldFactory.indexOpsService = indexOpsService 
    name
  }
  
  @Override
  Object createNode( Object name, Object value ){
    if( !fieldFactory.indexOpsService ) fieldFactory.indexOpsService = indexOpsService 
    if( null == value ) return name
    Field f = doc.getField( name )
    
    if( Collection.isAssignableFrom( value.getClass() ) ){
      value.eachWithIndex{ v, ix ->
        if( hasNoValue( f ) && !ix ) 
          fieldFactory.set f, v
        else
          doc.add fieldFactory.create( name, defaultStore, v )
      }
      
    }else if( hasNoValue( f ) )
      fieldFactory.set f, value
      
    else
      doc.add fieldFactory.create( name, defaultStore, value )
    name
  }

  @Override
  Object createNode( Object name, Map attrs ){
    if( !fieldFactory.indexOpsService ) fieldFactory.indexOpsService = indexOpsService 
    // prepare attributes    
    Store store = defaultStore
    if( null != attrs.store ){
      if( Boolean == attrs.store.getClass() ) store = attrs.store ? Store.YES : Store.NO
      else store = attrs.store
    }
    float boost = attrs.boost?.toFloat() ?: 1f
    if( attrs.containsKey( 'date' ) ){
      Resolution resolution = defaultDateResolution
      switch( attrs.resolution ){
        case Resolution: resolution = attrs.resolution; break
        case GString:
        case String: resolution = attrs.resolution.toUpperCase() as Resolution; break
      } 
      attrs.value = DateTools.dateToString( attrs.date, resolution )
    }
    
    Field f = doc.getField( name )

    String selector = 'value'
    def numericCandidate = FieldFactory.numericFields.keySet().intersect( attrs.keySet() )
    if( numericCandidate ) selector = numericCandidate.iterator().next()
      
    def val = attrs[ selector ]
    
    if( Collection.isAssignableFrom( val.getClass() ) || val.getClass().array ){
      if( val ) val.eachWithIndex{ v, ix ->
        if( hasNoValue( f ) && !ix ){
          fieldFactory.set f, v
          f.boost = boost
        }else{
          f = fieldFactory.create( name, store, attrs + [ (selector):v ] )
          if( f ){
            if( f.fieldType().numericType() ) numericFieldsJustAdded << name
            f.boost = boost
            doc.add f
          }
        }
      }
      
    }else if( hasNoValue( f ) ){
      fieldFactory.set f, attrs
      f.boost = boost
    
    }else{
      f = fieldFactory.create( name, store, attrs )
      if( f ){
        if( f.fieldType().numericType() ) numericFieldsJustAdded << name
        f.boost = boost
        doc.add f
      }
    }
    name
  }
  

  @Override
  Object createNode( Object name, Map attributes, Object value ){ name }
  
  @Override
  void setParent( Object parent, Object node ){}
  
  boolean hasNoValue( Field f ){
    if( !f ) return false
    if( null != f.numericValue() && !numericFieldsJustAdded.contains( f.name() ) || 
        null != f.binaryValue() && EMPTY_BINARY == f.binaryValue() ) return true 
    !f.stringValue() && !f.tokenStreamValue()
  }
  
  @Override
  protected void nodeCompleted( Object parent, Object node ) {}
  
  def reset(){
    Iterator iter = doc.iterator()
    numericFieldsJustAdded.clear()
    if( !iter.hasNext() ) return
    Field f
    while( iter.hasNext() && null != ( f = iter.next() ) ){
      if( 1 < doc.getFields( f.name ).size() || fieldsRemovalRegexp && f.name ==~ fieldsRemovalRegexp ){
        if( f.tokenStreamValue() ) f.tokenStream.reset()
        iter.remove()
      }else{
        if( null != f.numericValue() ) fieldFactory.set f, defaultNumericValue
        else if( f.binaryValue() ) f.setBytesValue EMPTY_BINARY 
        else if( f.stringValue() ) f.stringValue = ''
          
        if( f.tokenStreamValue() ){
          f.tokenStream.reset()
          f.tokenStream = null
        }
      }
    }
  }
  
}
