package grails.plugins.lucimple.builder

import grails.plugins.lucimple.IndexOpsService

import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.document.Field.Store
import org.apache.lucene.document.FieldType.NumericType as NT
import org.apache.lucene.document.*
import org.apache.lucene.index.FieldInfo.IndexOptions;
import org.apache.lucene.util.NumericUtils

/**
 * 
 * @author Injecteer
 *
 */
class FieldFactory {

  static final numericFields = [ integer:NT.INT, int:NT.INT, long:NT.LONG, float:NT.FLOAT, double:NT.DOUBLE ]
  
  IndexOpsService indexOpsService

  Field create( name, Store store, value ){
    Field f
    switch( value ){
      case null: return null
      case TokenStream: 
        f = new TextField( name, value )
        break
      case Number:
      case GString:
      case String: 
        f = new StringField( name, value.toString(), store )
        break
      case byte[]: 
        f = new StoredField( name, value )
        break
      case Map:
        if( value.analyzer && null == value.index ) value.index = true
        
        // numeric fields  
        if( !numericFields.keySet().disjoint( value.keySet() ) ){
          String type = numericFields.keySet().intersect( value.keySet() ).iterator().next()
          int precisionStep = null != value.precisionStep ? value.precisionStep.toInteger() : NumericUtils.PRECISION_STEP_DEFAULT
                  
          NT nt = numericFields[ type ]
          FieldType ft = new FieldType( numericPrecisionStep:precisionStep, stored:Store.YES == store, numericType:nt, 
                                        indexed:true, tokenized:true, omitNorms:true, indexOptions:IndexOptions.DOCS_ONLY )
          switch( nt ){
            case NT.INT: f = new IntField( name, (int)value[ type ], ft ); break
            case NT.LONG: f = new LongField( name, (long)value[ type ], ft ); break
            case NT.FLOAT: f = new FloatField( name, (float)value[ type ], ft ); break
            case NT.DOUBLE: f = new DoubleField( name, (double)value[ type ], ft ); break
          }
              
        // complex fields
        }else if( value.value || value.index ){
          TokenStream ts
          String val = value.value?.toString()
              
          switch( value.index ){
            case null:
              f = new StringField( name, val, store )
              break
                
            case Boolean:
              if( !value.index && Store.YES != store || !val ) return null
              if( value.index ){
                f = new TextField( name, val, store )
                if( value.analyzer ) f.tokenStream = indexOpsService.analyzeField( value.analyzer, name, val )
              }else
                f = new StoredField( name, val )
              break
                    
            case Number:
            case String:
            case GString:
              String v = value.index.toString()
              if( ( val || v ) && Store.YES == store ){
                f = new TextField( name, val ?: v, store )
                f.tokenStream = indexOpsService.analyzeField( value.analyzer, name, v )
              }else
                f = new TextField( name, v, store )
              break
                
            case TokenStream:
              ts = value.index
            case Map:
              if( !ts ) ts = value.index.tokenStream ?: indexOpsService.analyzeField( value.index.analyzer, name, value.index.value.toString() ?: val )
              if( val && Store.YES == store ){
                f = new TextField( name, val, store )
                f.tokenStream = ts
              }else
                f = new TextField( name, ts )
              break
          }
        }
        break
    }
    
    f?.fieldType()?.freeze()
    f
  }

  void set( Field f, value ){
    if( null == value ) return
    
    // numeric fields  
    if( f.fieldType().numericType() ){
      Number val
      if( Number.isAssignableFrom( value.getClass() ) )
        val = value
      else{
        String type = numericFields.keySet().intersect( value.keySet() ).iterator().next()
        val = value[ type ]
      }
      switch( f.fieldType().numericType() ){
        case NT.INT: f.intValue = (int)val; break
        case NT.LONG: f.longValue = (long)val; break
        case NT.FLOAT: f.floatValue = (float)val; break
        case NT.DOUBLE: f.doubleValue = (double)val; break
      }
      
    // single-valued fields
    }else if( !Map.isAssignableFrom( value.getClass() ) ){
      switch( value ){
        case TokenStream: f.tokenStream = value; break
        case Number:
        case GString:
        case String: f.stringValue = value.toString(); break
        case byte[]: f.setBytesValue value; break
      }
          
    // complex fields
    }else if( value.value || value.index ){
      
      TokenStream ts
      String val = value.value?.toString()
      if( val && null != f.stringValue() ) f.stringValue = val
              
      switch( value.index ){
        case Number:
        case String:
        case GString:
          f.tokenStream = indexOpsService.analyzeField( value.analyzer, f.name(), value.index ); break
        case TokenStream:
          f.tokenStream = value.index; break
        case Map:
          f.tokenStream = value.index.tokenStream ?: indexOpsService.analyzeField( value.index.analyzer, f.name(), value.index.value ?: val ); break
      }
    }
  }
  
}
